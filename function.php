<?
use Philo\Blade\Blade;

function View ($path,array $data = []) {
	$views = __DIR__ . '/Views';
$cache = __DIR__ . '/Cache';

    $blade = new Blade($views, $cache);
    
    echo $blade->view()->make($path,$data)->render();
} 

