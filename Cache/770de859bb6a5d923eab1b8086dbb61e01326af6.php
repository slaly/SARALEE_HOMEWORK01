<?php $__env->startSection('content'); ?>
<form action= "#" method="POST" id ="frmSearch">
    <div class = "container" align ="center">
            <h1><center>หน้าจอค้นหาข้อมูลบุคลากร</center></h1>  
    <div class ="row">
        <div class ="col-sm-2" align ="right">
            <label>รหัส</label>
        </div>
        <div class = "col-sm-4">
            <input type="text" name ="titleid" id ="titleid" class="form-control" >
        </div>
    </div> 
    
    <div class ="row">
            <div class ="col-sm-2" align ="right">
        <label>ระดับการศึกษา </label>
        <span class="asdf" style="position:relative">
            <select class="js-example-basic-single">
                  <option value="AL">Alabama</option>
                  <option value="WY">Wyoming</option>
            </select>
        </span> 
</div>
</div>

    
    <div class = "row">
        <div class ="col-sm-2" align ="right">
            <label>ชื่อ</label>
        </div>
        <div class ="col-sm-4">
            <input type="text" name ="fname" id ="fname" class="form-control">
        </div>
    </div>

    <div class = "row">
        <div class ="col-sm-2" align ="right">
            <label>นามสกุล</label>
        </div>
        <div class ="col-sm-4">
            <input type="text" name ="lname" id ="lname" class="form-control">
        <button type="button" class="seach" name= "seach">ค้นหา</button>
        </div>
  </div>


    <table id ="tbPerson" class="table table-striped table-bordered"  width= "100%" >
    <thead>
    <tr>
    <th>รหัส</th>
    <th>ชื่อ</th>
    <th>นามสกุล</th>
    <th>รายละเอียด</th>
    </tr>
    </thead>
    </table>
</div>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
   $(document).ready( function () {
   var table = $('#tbPerson').DataTable({
                ajax: {
                    url: "search",
                    type: "POST",
                    datatype: "JSON",
                    data: function (d) {
                        return $.extend({}, d, {
                            "person_id": $('#titleid').val(),
                            "Fname": $('#fname').val(),                            
                            "Lname": $('#lname').val(),                            
                        });
                    },
                    "dataSrc": function (json) {                       
                        if (json.data.length == 0) {
                            alert('ไม่พบข้อมูล');
                        } else {
                            return json.data;
                        }
                    },
                    error: function (xhr, error, thrown) {

                    }
                },
                deferLoading: 0,
                processing: true,
                serverSide: true,
                responsive: true,
                columns: [
                   
                    {
                        data: "person_id", 
                    },
                    {
                        data: "fname", render: function (data, type, row) {
                            return row["title_id"] + " " + data;
                        }
                    },
                    {
                        data: "lname"
                    },                    
                ],
                order: [0, "asc"],
                columnDefs: [
                    { className: "text-center", targets: [0] }
                ],
                bFilter: false,
                bLengthChange: false,
                pageLength: 10
            });
    });

$('#frmSearch').click(function() {

var in_person_id = $('#personId').val();
var in_fname = $('#fname').val();
var in_lname = $('#lname').val();
if(in_person_id != "" || in_fname != "" || in_lname !=""){
    table.ajax.reload();
}else if(in_person_id == ""){
    alert("กรุณาใส่เงื่อนไขอย่างน้อยหนึ่งเงื่อนไข")
    $('#personId').focus();
} 


});





</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>