<?php

require_once('vendor/autoload.php');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;  //เป็นการสร้าง entity manager 

$isDevMode = false;

$dbParam = array (
'driver' => 'mysqli',
'user' => 'root',
'password' => '',
'dbname' => 'hw01',
'host' => 'localhost',
'charset' => 'utf8',
);

$paths = array(__DIR__ . "/Model");

$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
$entityManager = EntityManager::create($dbParam,$config);

/*if($entityManager){
    echo "sucsess";
}else{
    echo "connection fail";
} */