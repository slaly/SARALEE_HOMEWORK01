<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="/homework01/css/bootstrap.min.css">
    <link rel="stylesheet" href="/homework01/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/homework01/css/dataTables.bootstrap4.min.css">
</head>
<body>
    @yield('content')

    <script src="/homework01/js/jquery.min.js"></script>
    <script src="/homework01/js/jquery.validate.min.js"></script>
    <script src="/homework01/js/additional-methods.min.js"></script>
    <script src="/homework01/js/bootstrap.min.js"></script>
    <script src="/homework01/js/jquery.dataTables.min.js"></script>
    <script src="/homework01/js/dataTables.bootstrap4.min.js"></script>
   <!-- <script src="/HOMEWORK01/js/push.min.js"></script> -->
    
    @yield('script')
</body>
</html>