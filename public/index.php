<?php
require '../vendor/autoload.php';
$router = new AltoRouter();
$router -> setBasePath('/homework01');
$router -> map('GET','/person','','person');
$router -> map('POST','/getdata','','getdata');
$router -> map('POST','/search','','search');
//$router -> map('POST','/sendsearch','','sendsearch');

$match = $router->match();
//var_dump($match);
//exit();
if($match){
    if($match['name']=="person"){
        require_once('../Controllers/PersonController.php');
        $person = new PersonController();
        $person->index();
    }else if($match['name']=="getdata"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->getData();
    }else if($match['name']=="search"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->search();
    }
   
}else {
    header($_SERVER['SERVER_PROTOCOL'] . '404 Not Found');
    echo "ไม่พบ Page";
    
}
